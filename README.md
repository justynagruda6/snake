# Snake
A classic snake game that I created as part of my JavaScript training. Technologies used: HTML (Canvas), SCSS, JavaScript.<br>
Live: [_https://snake-just-gru.netlify.app_](https://snake-just-gru.netlify.app).

You can choose from three difficulty levels. Use the arrows on the keyboard, or the arrows shown on the screen (on mobile devices) to control the snake and eat fruits.
Remember that you can't touch the wall. At any time, you can use the restart button and start the game from the beginning.

:snake: :snake: :snake: HAVE FUN!
