import {roundNumber} from "./helpers.js";

export class Board {
    constructor(config) {
        this.bgColor = "#606060";
        this.lineWidth = 1;
        this.config = config;
        this.bgCtx = this.config.bgCanvas.getContext("2d");
        this.canvasArr = [this.config.canvas, this.config.bgCanvas];
        this.setBoardSize();

        window.addEventListener('resize', () => {
            this.setBoardSize()
        })
    }

    setBoardSize() {
        const landsSize = roundNumber(
            window.innerWidth * 0.7,
            this.config.numberOfElements
        );
        const portSize = roundNumber(
            window.innerHeight * 0.7,
            this.config.numberOfElements
        );

        this.canvasArr.forEach((canv) => {
            if (window.innerHeight < window.innerWidth) {
                canv.height = landsSize;
                canv.width = landsSize;
            } else {
                canv.height = portSize;
                canv.width = portSize;
            }
        });
        this.config.elementSize =
            this.config.canvas.height / this.config.numberOfElements;

        this.lineWidth = window.innerWidth < 1100 ? 1 : 3;

        this.drawBackground();
        this.drawLines()
    }

    drawBackground() {
        this.bgCtx.fillStyle = this.bgColor;
        this.bgCtx.fillRect(
            0,
            0,
            this.config.bgCanvas.width,
            this.config.bgCanvas.height
        );
    }

    drawLines() {
        for (let i = 0; i <= this.config.numberOfElements; i++) {
            this.bgCtx.beginPath();
            this.bgCtx.moveTo(0, i * this.config.elementSize);
            this.bgCtx.lineTo(
                this.config.elementSize * this.config.numberOfElements,
                i * this.config.elementSize
            );
            this.bgCtx.moveTo(i * this.config.elementSize, 0);
            this.bgCtx.lineTo(
                i * this.config.elementSize,
                this.config.elementSize * this.config.numberOfElements
            );
            this.bgCtx.stroke();
            this.bgCtx.lineWidth = this.lineWidth;
        }
    }


}
