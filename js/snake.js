import {randomizeCoordinates, randomizeNumber} from "./helpers.js";

export class Snake {
    constructor(config) {
        this.config = config;
        this.ctx = this.config.canvas.getContext("2d");
        this.snake = [];
        this.snakeDirection = "";
        this.resetSnake();
        this.createListener();
    }

    createListener() {
        window.addEventListener("keyup", (e) => {
            this.turnSnake(e.key);
        });
    }

    resetSnake() {
        this.createSnake();
        this.drawSnake();
    }

    createSnake() {
        this.randomizeSnakeDirection();
        this.snake = [randomizeCoordinates(5, this.config.numberOfElements - 5)];
        this.shouldDeleteLastElement = false;
        this.addSnakeElement();
        this.shouldDeleteLastElement = true;
    }

    drawSnake() {
        this.snake.forEach((el) => {
            this.ctx.fillStyle = "#D6ED17";
            this.ctx.fillRect(
                el.x * this.config.elementSize,
                el.y * this.config.elementSize,
                this.config.elementSize,
                this.config.elementSize
            );
            this.ctx.strokeRect(
                el.x * this.config.elementSize,
                el.y * this.config.elementSize,
                this.config.elementSize,
                this.config.elementSize
            );
        });
        this.canPressButton = true;
    }

    addSnakeElement() {
        this.snake.unshift(this.createNewSnakePoint());
    }

    createNewSnakePoint() {
        const point = {...this.snake[0]};
        switch (this.snakeDirection) {
            case "right":
                point.x++;
                break;

            case "left":
                point.x--;
                break;

            case "down":
                point.y++;
                break;

            case "up":
                point.y--;
                break;
        }
        return point;
    }

    moveSnake() {
        if (this.shouldDeleteLastElement) {
            this.snake.pop();
        }
        this.addSnakeElement();
        this.drawSnake();
        this.shouldDeleteLastElement = true;
    }

    changeSnakeDirection(arrowKey) {
        if (arrowKey === "ArrowLeft" && this.snakeDirection !== "right") {
            this.snakeDirection = "left";
        } else if (arrowKey === "ArrowRight" && this.snakeDirection !== "left") {
            this.snakeDirection = "right";
        } else if (arrowKey === "ArrowUp" && this.snakeDirection !== "down") {
            this.snakeDirection = "up";
        } else if (arrowKey === "ArrowDown" && this.snakeDirection !== "up") {
            this.snakeDirection = "down";
        }
    }

    turnSnake(arrowKey) {
        if (!this.canPressButton) {
            return;
        }
        this.canPressButton = false;
        this.changeSnakeDirection(arrowKey);
    }

    extendSnake() {
        this.shouldDeleteLastElement = false;
    }

    isGameOver() {
        const newSnakePoint = this.createNewSnakePoint();
        return (
            this.snake.some(
                (el) => newSnakePoint.x === el.x && newSnakePoint.y === el.y
            ) ||
            0 > newSnakePoint.y ||
            newSnakePoint.y >= this.config.numberOfElements ||
            0 > newSnakePoint.x ||
            newSnakePoint.x >= this.config.numberOfElements
        );
    }

    randomizeSnakeDirection() {
        const direction = ["up", "down", "left", "right"];
        this.snakeDirection = direction[randomizeNumber(0, 4)];
    }
}
