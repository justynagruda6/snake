import {isTouchEnabled} from "./helpers.js";

export class Interface {
    constructor(config) {
        this.config = config;
        this.timeToStart = 3;
        this.counterInterval = null;
        this.onStart = () => {
        };
        this.onRestart = () => {
        };
        this.onArrowTouch = (arrowKey) => {
        };

        this.getHtmlElements();

        this.showArrows();
        this.addListeners();
    }

    getHtmlElements() {
        this.pointsElement = document.getElementById("points");
        this.gameOverElement = document.getElementById("game-over");
        this.counterElement = document.getElementById("counter");
        this.arrowsContainer = document.querySelector(".arrows-container");
        this.arrows = document.querySelectorAll(".arrows button");
        this.chooseLevelContainer = document.getElementById("choose-level");
        this.chooseLevelBtn = document.querySelectorAll(".level");
        this.restartBtn = document.getElementById("restart");
        this.startBtn = document.getElementById("start");
    }

    addListeners() {
        this.chooseLevelBtn.forEach((level) => {
            level.addEventListener("click", (e) => {
                this.chooseLevelClick(e.target);
            });
        });

        this.restartBtn.addEventListener("click", () => {
            this.gameOverElement.style.display = "none";
            this.chooseLevelContainer.style.display = "block";
            this.resetCounter();
            this.setPoints(0);
            this.onRestart();
        });

        this.startBtn.addEventListener("click", () => {
            this.chooseLevelContainer.style.display = "none";
            this.turnCounter();
        });

        if (isTouchEnabled()) {
            this.arrows.forEach((arrow) => {
                arrow.addEventListener("touchstart", () => {
                    this.onArrowTouch(arrow.getAttribute("data-arrow-key"));
                });
            });
        }
    }

    turnCounter() {
        this.showCounter(this.timeToStart);
        this.counterInterval = setInterval(() => {
            if (this.timeToStart > 1) {
                this.timeToStart--;
                this.showCounter(this.timeToStart);
            } else {
                this.resetCounter();
                this.onStart();
            }
        }, 1000);
    }

    resetCounter() {
        clearInterval(this.counterInterval);
        this.counterElement.style.display = "none";
        this.timeToStart = 3;
    }

    showGameOver(points) {
        this.gameOverElement.innerHTML = `GAME OVER!  <br> ZDOBYTE PUNKTY : ${points}`;
        this.gameOverElement.style.display = "flex";
    }

    showCounter(number) {
        this.counterElement.innerHTML = number;
        this.counterElement.style.display = "flex";
    }

    setPoints(points) {
        this.pointsElement.innerHTML = `PUNKTY: ${points}`;
    }

    showArrows() {
        this.arrowsContainer.style.display = isTouchEnabled() ? "flex" : "none";
    }

    chooseLevelClick(el) {
        this.chooseLevelBtn.forEach((level) => {
            level.classList.remove("active");
        });
        el.classList.add("active");

        switch (el.id) {
            case "easy":
                this.config.intervalTime = 250;
                break;
            case "medium":
                this.config.intervalTime = 200;
                break;
            case "hard":
                this.config.intervalTime = 150;
                break;
        }
    }
}
