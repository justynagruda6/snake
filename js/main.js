import {Interface} from "./interface.js";
import {Board} from "./board.js";
import {Snake} from "./snake.js";
import {Fruit} from "./fruit.js";
import {setVh} from "./helpers.js";

class Game {
    constructor() {
        setVh();
        this.config = {
            canvas: document.getElementById("canvas"),
            bgCanvas: document.getElementById("bg-canvas"),
            numberOfElements: 30,
            intervalTime: 250,
        };
        this.points = 0;
        this.interval = null;
        this.createListener();
        this.initGame();
    }

    initGame() {
        this.interface = new Interface(this.config);
        this.interface.onStart = this.startGame.bind(this);
        this.interface.onRestart = this.onRestart.bind(this);

        this.board = new Board(this.config);
        this.fruit = new Fruit(this.config);
        this.snake = new Snake(this.config);
        this.interface.onArrowTouch = this.snake.turnSnake.bind(this.snake);
    }

    onRestart() {
        this.config.canvas.height += 0;
        clearInterval(this.interval);
        this.points = 0;
        this.fruit.reset();
        this.snake.resetSnake();
    }

    createListener() {
        window.addEventListener("resize", () => {
            setVh();
            this.board.setBoardSize();
            this.snake.drawSnake();
            this.fruit.drawFruits();
        });
    }

    startGame() {
        this.interval = setInterval(() => {
            this.handleMove();
        }, this.config.intervalTime);
    }

    handleMove() {
        if (!this.snake.isGameOver()) {
            this.config.canvas.height += 0;
            this.fruit.drawFruits();
            this.snake.moveSnake();
            this.eatFruit();
        } else {
            clearInterval(this.interval);
            this.interface.showGameOver(this.points);
        }
    }

    eatFruit() {
        const isEaten = this.fruit.handleEatFruit(
            this.snake.snake[0].x,
            this.snake.snake[0].y
        );
        if (isEaten) {
            this.snake.extendSnake();
            this.points++;
            this.interface.setPoints(this.points);
        }
    }
}

new Game();
