export function isTouchEnabled() {
    return (
        "ontouchstart" in window ||
        navigator.maxTouchPoints > 0 ||
        navigator.msMaxTouchPoints > 0
    );
}

export function randomizeNumber(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

export function randomizeCoordinates(min, max) {
    return {
        x: randomizeNumber(min, max),
        y: randomizeNumber(min, max),
    };
}

export function roundNumber(num, elem) {
    return Math.round(num / elem) * elem;
}

export function setVh() {
    const vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty("--vh", `${vh}px`);
}
