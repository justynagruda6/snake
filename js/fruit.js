import {randomizeCoordinates} from "./helpers.js";

export class Fruit {
    constructor(config) {
        this.config = config;
        this.ctx = this.config.canvas.getContext("2d");
        this.reset();
    }

    createFruitObject() {
        return randomizeCoordinates(0, this.config.numberOfElements);
    }

    handleEatFruit(snakeX, snakeY) {
        const fruitIdx = this.fruits.findIndex(fruit => fruit.x === snakeX && fruit.y === snakeY);

        if (fruitIdx >= 0) {
            this.fruits.splice(fruitIdx, 1);
            this.fruits.push(this.createFruitObject());
        }
        return fruitIdx >= 0;
    }

    drawFruits() {
        this.fruits.forEach((fruit) => {
            this.ctx.fillStyle = "#D01460";
            this.ctx.fillRect(
                fruit.x * this.config.elementSize,
                fruit.y * this.config.elementSize,
                this.config.elementSize,
                this.config.elementSize
            );
            this.ctx.strokeRect(
                fruit.x * this.config.elementSize,
                fruit.y * this.config.elementSize,
                this.config.elementSize,
                this.config.elementSize
            );
        });
    }

    reset() {
        this.fruits = [this.createFruitObject()];
        this.drawFruits();
    }
}
